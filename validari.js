
$("#form").submit(function (e) {
    e.preventDefault();

    /* $("#loading").show(); */
    document.getElementById("save").disabled = true;
    setTimeout(function() {
    document.getElementById("save").disabled = false;
    }, 5000);

    var formObj = $(this);
    var formURL = formObj.attr("action");
    var formData = new FormData(this);
    $.ajax({
        url: formURL,
        type: 'POST',
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        error: function (jqXHR) {
           
            var data = JSON.parse(jqXHR.responseText);
            const errors_container = $('#errors-container');
            errors_container.empty();

            for(key in data){
                if(Array.isArray(data[key])){

                    for(i=0;i<data[key].length;i++){
                        errors_container.append('<center><h5 class="center">' + data[key][i] + '</h5></center>');
                    }
                
                } else {
                    errors_container.append('<center><h5 class="center">' + data[key] + '</h5></center>');
                }
            }

            $(window).scrollTop(0);
        },
        success: function (jqXHR) {
            /* $("#loading").hide(); */
            var body = $('#form');
            
            $("#errors-container").empty();

            body.parent().append('<center><h1 style="color: white; padding-bottom: 61vh;">Felicitări! Ai fost înregistrat cu succes! Verifică-ți email-ul pentru a intra în posesia codurilor QR necesare în ziua evenimentului.</h1></center>');
            body.remove();
            $(window).scrollTop(0);

        }
    });
});